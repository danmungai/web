import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseurlService {

  constructor() { }
  baseUrl: string = 'https://localhost:8080/api/v1';

  getUrl(): string{
    return this.baseUrl;
  }
}

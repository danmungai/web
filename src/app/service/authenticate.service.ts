import { Observable, ReplaySubject } from 'rxjs';
import { Injectable } from '@angular/core';

export interface IUser{
  username:string;
  id : number;
  Firstname: string;
  lastname: string;

}

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  constructor() {
    if(localStorage.getItem("user")){
      this.user = JSON.parse(localStorage.getItem("user"));
      this.loggedIn = true;
    }else{
      this.loggedIn= false;
    }
   }
   loggedOut: ReplaySubject<boolean> = new ReplaySubject </boolean>();

   private user: IUser;
   private loggedIn: boolean;

   isLoggedIn(): boolean{
     return this.loggedIn;
   }

   loginAuthenticatedUser(authenticatedUser:IUser, stayLoggedIn: boolean): void{
     this.user = authenticatedUser;
     this.loggedIn = true;
     if(stayLoggedIn){
       localStorage.setItem("user", JSON.stringify(authenticatedUser));
     }
   }
   resaveUser(authenticatedUser: IUser){
     this.user = authenticatedUser;
     if(localStorage.getItem("user")){
       localStorage.setItem("user", JSON.stringify(authenticatedUser));
     }
   }

   getAuthenticatedUser(): IUser{
     return this.user:
   }

   logout():void{
     this.user = undefined;
     this.loggedIn=false;
     localStorage.clear();
     this.loggedOut.next(true):
   }
}
